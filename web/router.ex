defmodule FanimaidButler.Router do
  use FanimaidButler.Web, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", FanimaidButler do
    pipe_through :browser # Use the default browser stack

    get "/", PageController, :index

    resources "/maids", MaidController
    post "/maids/:id/check-in", MaidController, :check_in
    post "/maids/:id/check-out", MaidController, :check_out

    resources "/tables", TableController

    get "/parties/clear", PartyController, :clear
    post "/parties/clear", PartyController, :clear
    resources "/parties", PartyController

    get "/reservations/new/:table_id", ReservationController, :new
    get "/reservations/clear", ReservationController, :clear
    post "/reservations/clear", ReservationController, :clear
    resources "/reservations", ReservationController
  end

  # Other scopes may use custom stacks.
  # scope "/api", FanimaidButler do
  #   pipe_through :api
  # end
end
