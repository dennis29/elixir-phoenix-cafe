defmodule FanimaidButler.Reservation do
  use FanimaidButler.Web, :model

  alias FanimaidButler.Repo
  alias FanimaidButler.Party
  alias FanimaidButler.Table

  schema "reservations" do
    field :size, :integer
    field :shinkansen, :boolean, default: false
    field :staff, :boolean, default: false
    field :time_in, :utc_datetime
    field :time_out, :utc_datetime
    field :notes, :string
    field :table_number, :string

    timestamps()

    belongs_to :party, FanimaidButler.Party
    belongs_to :maid, FanimaidButler.Maid
  end

  def booking_changeset(struct, params \\ %{}) do
    struct
    |> changeset(params)
    |> put_change(:time_in, Ecto.DateTime.utc)
  end

  def clearing_changeset(struct, params \\ %{}) do
    struct
    |> changeset(params)
    |> put_change(:time_out, Ecto.DateTime.utc)
    |> put_change(:party_id, nil)
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:size, :shinkansen, :staff, :time_in, :time_out, :notes, :table_number, :party_id, :maid_id])
    |> cast_assoc(:maid)
    |> cast_assoc(:party)
    |> validate_matching_party()
    |> validate_party_available()
    |> validate_party_size()
    |> validate_required([:size, :shinkansen, :staff, :maid_id, :table_number, :party_id])
  end

  def validate_matching_party(changeset \\ []) do 
    if get_change(changeset, :party_id) && Repo.get!(Party,get_change(changeset, :party_id)).table_id != Repo.get_by!(Table, table_number: get_change(changeset, :table_number)).id do
      add_error(changeset, :party_id, "Scanned party does not match table!")
    else
      changeset
    end
  end

  def validate_party_size(changeset \\ []) do
    if get_change(changeset, :size) do
      table = Repo.get_by!(Table, table_number: get_change(changeset, :table_number)) |> Repo.preload [parties: :reservation]
      table_parties = table.parties
      full_seats = Enum.reduce(table_parties, 0, fn(x, acc) -> if x.reservation do x.reservation.size + acc else acc end end)
      max_seats = table.max_capacity
      
      if(get_change(changeset, :size) > (max_seats - full_seats)) do
        add_error(changeset, :size, "Specified party size is too large for this table!")
      else
        changeset
      end
    else
      changeset
    end
  end

  def validate_party_available(changeset \\ []) do
    if get_change(changeset, :party_id) do
      party = Repo.get!(Party, get_change(changeset, :party_id)) |> Repo.preload [reservation: :maid]
      if party.reservation do
        add_error(changeset, :party_id, "Barcode already assigned to #{party.reservation.maid.name}")
      else
        changeset
      end
    else
      changeset
    end
  end

end
