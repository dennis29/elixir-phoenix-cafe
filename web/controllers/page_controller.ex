defmodule FanimaidButler.PageController do
  use FanimaidButler.Web, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end
end
