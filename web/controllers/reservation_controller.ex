defmodule FanimaidButler.ReservationController do
  use FanimaidButler.Web, :controller

  alias FanimaidButler.Reservation
  alias FanimaidButler.Maid
  alias FanimaidButler.Party
  alias FanimaidButler.Table

  def index(conn, %{"page" => page} = params) do
    goshujinsama = Repo.aggregate(Reservation, :sum, :size)

    page =
      Reservation
      |> order_by(desc: :id)
      |> preload([:maid, party: :table])
      |> FanimaidButler.Repo.paginate(page: page)

    render(conn, "index.html",
      url: "/reservations",
      reservations: page.entries,
      page_number: page.page_number,
      page_size: page.page_size,
      total_pages: page.total_pages,
      total_entries: page.total_entries,
      goshujinsama: goshujinsama)
  end

  def index(conn, _params) do
    redirect(conn, to: reservation_path(conn, :index, page: 1))
  end

  def new(conn, %{"table_id" => table_id}) do
    all_maids = Repo.all(from maid in Maid, where: maid.status == "present") |> Repo.preload :reservations
    maids = Enum.filter(all_maids, fn(x) -> !Enum.any?(x.reservations, fn(y) -> is_nil(y.time_out) end) end)
    table = Repo.get!(Table, table_id)
    changeset = Reservation.changeset(%Reservation{})
    render(conn, "new.html", maids: maids, table: table, changeset: changeset)
  end

  def create(conn, %{"reservation" => reservation_params}) do
    changeset = Reservation.booking_changeset(%Reservation{}, reservation_params)

    case Repo.insert(changeset) do
      {:ok, reservation} ->
        conn
        |> put_flash(:info, "Reservation created successfully.")
        |> redirect(to: table_path(conn, :index))
      {:error, changeset} ->
        all_maids = Repo.all(from maid in Maid, where: maid.status == "present") |> Repo.preload :reservations
        maids = Enum.filter(all_maids, fn(x) -> !Enum.any?(x.reservations, fn(y) -> is_nil(y.time_out) end) end)
        table = Repo.get_by!(Table, table_number: reservation_params["table_number"])
        render(conn, "new.html", maids: maids, table: table, changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    reservation = Repo.get!(Reservation, id) |> Repo.preload [:maid, party: :table]
    render(conn, "show.html", reservation: reservation)
  end

  def edit(conn, %{"id" => id}) do
    reservation = Repo.get!(Reservation, id)
    changeset = Reservation.changeset(reservation)
    render(conn, "edit.html", reservation: reservation, changeset: changeset)
  end

  def update(conn, %{"id" => id, "reservation" => reservation_params}) do
    reservation = Repo.get!(Reservation, id)
    changeset = Reservation.changeset(reservation, reservation_params)

    case Repo.update(changeset) do
      {:ok, reservation} ->
        conn
        |> put_flash(:info, "Reservation updated successfully.")
        |> redirect(to: reservation_path(conn, :show, reservation))
      {:error, changeset} ->
        render(conn, "edit.html", reservation: reservation, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    reservation = Repo.get!(Reservation, id)

    # Here we use delete! (with a bang) because we expect
    # it to always work (and if it does not, it will raise).
    Repo.delete!(reservation)

    conn
    |> put_flash(:info, "Reservation deleted successfully.")
    |> redirect(to: reservation_path(conn, :index))
  end

  def clear(conn, %{"reservation" => %{"party_id" => ""}}) do
    conn
    |> put_flash(:error, "Must scan a barcode.")
    |> redirect(to: reservation_path(conn, :clear))
  end

  def clear(conn, %{"reservation" => %{"party_id" => party_id}}) do
    reservation = Repo.get_by(Reservation, party_id: party_id)
    if reservation do
      changeset = Reservation.clearing_changeset(reservation)
      case Repo.update(changeset) do
        {:ok, reservation} ->
          conn
          |> put_flash(:info, "Reservation cleared successfully.")
          |> redirect(to: reservation_path(conn, :show, reservation))
        {:error, changeset} ->
          render(conn, "clear.html", reservation: reservation, changeset: changeset)
      end
    else
      conn
      |> put_flash(:error, "The party you scanned was already empty :(")
      |> redirect(to: reservation_path(conn, :clear))
    end
  end

  def clear(conn, _params) do
    changeset = Reservation.changeset(%Reservation{})
    render(conn, "clear.html", changeset: changeset)
  end
end
