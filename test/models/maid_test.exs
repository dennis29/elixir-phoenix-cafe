defmodule FanimaidButler.MaidTest do
  use FanimaidButler.ModelCase

  alias FanimaidButler.Maid

  @valid_attrs %{goshujinsama: 42, logged_hours: 120.5, name: "some name", status: "some status", tables: 42}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = Maid.changeset(%Maid{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = Maid.changeset(%Maid{}, @invalid_attrs)
    refute changeset.valid?
  end
end
