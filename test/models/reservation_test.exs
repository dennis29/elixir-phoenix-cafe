defmodule FanimaidButler.ReservationTest do
  use FanimaidButler.ModelCase

  alias FanimaidButler.Reservation

  @valid_attrs %{notes: "some notes", shinkansen: true, size: 42, staff: true, time_in: "2010-04-17 14:00:00.000000Z", time_out: "2010-04-17 14:00:00.000000Z"}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = Reservation.changeset(%Reservation{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = Reservation.changeset(%Reservation{}, @invalid_attrs)
    refute changeset.valid?
  end
end
